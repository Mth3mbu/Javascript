class UI {
    GetUI(user) {
     const parent = document.getElementById('usercontent');
     parent.innerHTML = `
     <div class="card" style="width: 18rem;">
     <img class='card-img-top' src=${user.avatar_url} alt='Card image cap'>
     <div class="card-body">
     <ul class="list-group list-group-flush">
     <li class="list-group-item">{${user.login}}</li>
     <li class="list-group-item">Works for ${user.company}</li>
     <li class="list-group-item"> from ${user.location}</li>
     <li class="list-group-item">Bio ${user.bio}</li>
     </ul>
     </div>
    </div>
    
     `;
     console.log(parent);
    }
}