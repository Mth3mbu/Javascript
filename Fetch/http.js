/**
 * LIbrary making HTTP Requests
 * @version 1.0.0
 * @author Jack Mthembu
 * @licence KCP 
 */
class EasyHTTP {
    //using EasyHttp
    get(url) {
        return new Promise((resolve, reject) => {
            fetch(url)
                .then(res => res.json())
                .then(data => resolve(data))
                .catch(error => reject(error));
        });
    }
    //get data using asynch
    async Get(url) {
        const response = await fetch(url);
        const data = await response.json();
        return data;
    }
    //Post using EasyHTTP
    post(url, data) {
        return new Promise((resolve, reject) => {
            fetch(url, {
                method: 'POST',
                headers: { 'Content-type': 'application/json' },
                body: JSON.stringify(data)
            })
                .then(res => res.json())
                .then(data => resolve(data))
                .catch(err => reject(err));
        });
    }
    //Post data using async
    async Post(url, data) {
        const response = await fetch(url, {
            method: 'POST',
            headers: { 'Content-type': 'application/json' },
            body: JSON.stringify(data)
        });
        const resData = await response.json();
        return resData;
    }

    put(url, data) {
        return new Promise((resolve, reject) => {
            fetch(url, {
                method: 'PUT',
                headers: { 'Content-type': 'application/json' },
                body: JSON.stringify(data)
            })
                .then(res => res.json())
                .then(data => resolve(data))
                .catch(err => reject(err));
        });
    }
    //Delete using EasyHTTP
    delete(url) {
        return new Promise((resolve, reject) => {
            fetch(url, {
                method: 'DELETE',
                headers: { 'Content-type': 'application/json' }
            })
                .then(res => res.json())
                .then(data => resolve('User deleted succesfully'))
                .catch(err => reject(err));
        });
    }
    //Delete data using async
    async Delete(url) {
        const response = await fetch(url, {
            method: 'DELETE',
            headers: { 'Content-type': 'application/json' }
        })
        const resData = await 'User deleted succesfully';
        return resData;
    }
}