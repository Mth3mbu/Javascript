const http = new EasyHTTP;

document.querySelector('.btn').addEventListener('click', (e) => {
    http.Get("https://jsonplaceholder.typicode.com/users")
        .then(data => {
            const table = document.querySelector('table');
            data.forEach(user => {
                CreateUI(user, table);
            });
        })
        .catch(err => console.log(err));
    e.preventDefault();
    document.querySelector('.btn').disabled = true;
});

document.getElementById('save').addEventListener('click', (e) => {
    const data = { name: 'Jack', username: 'Jackaranda6', email: 'jack.mthembu@Kzn' };

    http.Post('https://jsonplaceholder.typicode.com/users', data)
        .then(data => console.log(data))
        .catch(err => console.log(err));
      e.preventDefault();
})


document.getElementById('udpate').addEventListener('click', (e) => {
    const data = { name: 'Jack', username: 'Jackaranda6', email: 'jack.mthembu@Kzn' };

    http.put('https://jsonplaceholder.typicode.com/users/1', data)
        .then(data => console.log(data))
        .catch(err => console.log(err));
    e.preventDefault();
})

document.getElementById('delete').addEventListener('click', (e) => {
    http.Delete('https://jsonplaceholder.typicode.com/users/5')
        .then(data => console.log(data))
        .catch(err => console.log(err));
    e.preventDefault();
})

const CreateUI = (user, table) => {
      //Object destructuring
      const {name,email,address}=user;
    
    const row = document.createElement('tr');
    row.innerHTML = `<td>${name}</td>
        <td>${email}</td>
        <td>${address.street},${address.city}</td>
        `;
    table.appendChild(row);
};