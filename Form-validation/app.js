document.getElementById('username').addEventListener('blur', (e) => {
    let username = document.getElementById('username');
    const reg = /^[a-zA-Z]{5,10}$/;
    if (!reg.test(username.value)) {
        username.classList.add('is-invalid');
    } else {
        username.classList.remove('is-invalid');
    }
    e.preventDefault();
});

document.getElementById('phone').addEventListener('blur', (e) => {
    let username = document.getElementById('phone');
    const reg = /^[0-9]{10}$/;
    if (!reg.test(username.value)) {
        username.classList.add('is-invalid');
    } else {
        username.classList.remove('is-invalid');
    }
    e.preventDefault();
});

document.getElementById('email').addEventListener('blur', (e) => {
    let username = document.getElementById('email');
    const reg = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
    if (!reg.test(username.value)) {
        username.classList.add('is-invalid');
    } else {
        username.classList.remove('is-invalid');
    }
    e.preventDefault();
});