class UI {
    constructor() {
        this.city = document.getElementById('w-city');
        this.state = document.getElementById('w-state');
        this.icon = document.getElementById('w-icon');
        this.weather = document.getElementById('weather');
        this.humidity = document.getElementById('w-humidity');
        this.feelslike=document.getElementById('w-feelslike');
        this.windspeed=document.getElementById('w-windspeed');
        this.temparature=document.getElementById('w-temparature');
        this.dewpoint=document.getElementById('w-dewpoint');
        this.date=document.getElementById('date');
        this.wind=document.getElementById('w-wind');
    }

    setUi(weather) {
        this.city.textContent = weather.display_location.full;
        this.state.textContent = weather.display_location.state;
        this.icon.setAttribute('src', weather.icon_url);
        this.weather.textContent = weather.weather;
        this.humidity.textContent =`Humidity ${weather.relative_humidity}`;
        this.feelslike.textContent=`Feels like ${weather.feelslike_string}`
        this.windspeed.textContent=`Wind speed ${weather.wind_string}`
        this.temparature.textContent=`Temperature ${weather.temperature_string}`;
        this.dewpoint.textContent=`Dew point ${weather.dewpoint_string}`;
        this.date.textContent=`Date ${weather.local_time_rfc822}`;
        this.wind.textContent=`Wind ${weather.wind_kph} KPH`
    }

    clearField(){
        document.getElementById('city').value="";
    }

    showAlert(){
        document.querySelector('.alert').style.display='block';  
        setTimeout(() => {
            document.querySelector('.alert').style.display='none';  
        }, 3000);
    }
}