const localstorage = new LocalStorage();

GetWeather(localstorage.getCity());

document.querySelector('.alert').style.display = 'none';
document.querySelector('form').addEventListener('submit', (e) => {

    let city = document.getElementById('city').value;
    localstorage.setCity(city);

    GetWeather(city);

    e.preventDefault();
});

function GetWeather(location) {

    const weather = new Weather(location);

    const ui = new UI();

    weather.get().then(data => {

        ui.setUi(data);

    }).catch(err => {
        ui.showAlert();
        console.log(err);
    })

    ui.clearField();
}