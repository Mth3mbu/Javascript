class LocalStorage {

    constructor() {

        this.city = localStorage.getItem('city');

        if (this.city === null) {

            this.city = 'Durban';
        }
    }

    getCity() {
        return this.city;
    }

    setCity(city) {
        localStorage.setItem('city', city);
    }
}