const conatainer = document.getElementById('Profile');

const data = [
    {
        name: 'jack',
        age: 24,
        gender: 'male',
        lookingfor: 'female',
        location: 'durban',
        image: 'https://randomuser.me/api/portraits/men/82.jpg'
    },
 
    {
        name: 'Sandra',
        age: 24,
        gender: 'female',
        lookingfor: 'male',
        location: 'Balito',
        image: 'https://randomuser.me/api/portraits/women/82.jpg'
    }

];
const profiles = NextProfile(data);

document.getElementById('next').addEventListener('click', (e) => {

    let { image, name, age, gender, lookingfor } = profiles.next().value;

    console.log(image);
    conatainer.innerHTML = `
    <div class="form-group"><img src="${image}"/></div>
    <ul class="list-group">
     <li class="list-group-item">Name: ${name}</li>
     <li class="list-group-item">Age: ${age}</li>
     <li class="list-group-item">Gender: ${gender}</li>
     <li class="list-group-item">Looking for: ${lookingfor}</li>
  </ul>`
    e.preventDefault();
});

function NextProfile(profiles) {
    let nextIndex = 0;
    return {
        next: function () {
            return nextIndex < profiles.length ? { value: profiles[nextIndex++], done: false } : { done: true };
        }
    };
}