class Book {
    constructor(name, author, isbn) {
        this.name = name;
        this.author = author;
        this.isbn = isbn;
    };
}

class UI {
    DisplayTableData(book) {
        const list = document.getElementById('tbl-list');
        const row = document.createElement('tr');
        row.innerHTML = `<td>${book.name}</td>
                       <td>${book.author}</td>
                       <td>${book.isbn}</td>
                       <td><a href="#" class="delete"></a></td>
        `
        list.appendChild(row);
    };

    ShowAlert(message, className) {

        const div = document.getElementById('alert');
        div.style.display = 'block';
        div.className = `alert ${className}`;
        div.innerHTML = message;

        setTimeout(function () {
            document.getElementById('alert').style.display = 'none';
        }, 3000);
    }

    ClearFields() {
        document.getElementById('title').value = "";
        document.getElementById('author').value = "";
        document.getElementById('isbn').value = "";
    }

    HideElements(className) {
        const list = document.getElementById('tbl-list');
        list.style.display = className;
    }
}

const form = document.getElementById('book-list');
const title = document.getElementById('title'),
    author = document.getElementById('author'),
    isbn = document.getElementById('isbn');

let ui = new UI();
ui.HideElements('none');

form.addEventListener('submit', function (e) {
    let book = new Book(title.value, author.value, isbn.value);

    if (book.name != "" && book.author != "" && book.isbn != "") {
        ui.DisplayTableData(book);
        ui.HideElements('block')
        ui.ClearFields();
        ui.ShowAlert('Book saved', 'alert-success');
    }
    else {
        ui.ShowAlert('Please fill the empty fields', 'alert-error')
    }

    e.preventDefault();
})